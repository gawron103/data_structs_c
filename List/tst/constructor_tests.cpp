#include "gtest/gtest.h"

#ifdef __cplusplus
extern "C" {
#include "list.h"
}
#else
#include "list.h"
#endif

TEST(List_Init_Tests, initCheckVal) {
    CheckT init_retVal = E_NOT_OK;

    ListT list;

    init_retVal = init(&list);

    EXPECT_EQ(init_retVal, E_OK);
}

TEST(List_Init_Tests, structValuesAfterInit) {
    ListT list;

    (void)init(&list);

    EXPECT_TRUE(list.root == NO_MEM_ALLOCATED);
    EXPECT_EQ(list.size, SIZE_INIT_VAL);
}

TEST(List_Init_Tests, structInitWithAssignedVals) {
    const size_t WANTED_SIZE = 10u;

    ListT list = {
        .root = NO_MEM_ALLOCATED,
        .size = WANTED_SIZE
    };

    (void)init(&list);

    EXPECT_TRUE(list.root == NO_MEM_ALLOCATED);
    EXPECT_EQ(list.size, SIZE_INIT_VAL);
}
