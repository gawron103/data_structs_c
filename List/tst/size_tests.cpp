#include "gtest/gtest.h"

extern "C" {
#include "list.h"
}

TEST(List_Size_Tests, checkSizeAfterInit) {
    SizeT listSize;

    ListT list;

    (void)init(&list);

    listSize = size(&list);

    EXPECT_EQ(listSize, SIZE_INIT_VAL);
}

TEST(List_Size_Tests, checkSizeAfterDeinit) {
    SizeT listSize;

    ListT list;

    (void)init(&list);
    (void)deinit(&list);

    listSize = size(&list);

    EXPECT_EQ(listSize, SIZE_INVALID_VAL);
}

TEST(List_Size_Tests, checkSizeAfterPushBack) {
    const size_t WANTED_SIZE = 5u;
    SizeT listSize;

    ListT list;

    (void)init(&list);
    
    for(int i = 0; i < WANTED_SIZE; i++) {
        push_back(&list, i);
    }

    listSize = size(&list);

    EXPECT_EQ(listSize, WANTED_SIZE);
}

TEST(List_Size_Tests, checkSizeAfterPushFront) {
    const size_t WANTED_SIZE = 3u;
    SizeT listSize;

    ListT list;

    (void)init(&list);
    
    for(int i = 0; i < WANTED_SIZE; i++) {
        push_front(&list, i);
    }

    listSize = size(&list);

    EXPECT_EQ(listSize, WANTED_SIZE);
}

TEST(List_Size_Tests, checkSizeWhenNull) {
    SizeT listSize;

    listSize = size(NO_MEM_ALLOCATED);

    EXPECT_EQ(listSize, SIZE_INVALID_VAL);
}
