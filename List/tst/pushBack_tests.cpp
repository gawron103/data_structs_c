#include "gtest/gtest.h"

extern "C" {
#include "list.h"
}

TEST(List_PushBack_Tests, pushCheckVal) {
    const int WANTED_SIZE = 3;
    CheckT pushRetVal = E_NOT_OK;
    const int TEST_VAL = 7;

    ListT list;

    (void)init(&list);
    pushRetVal = push_back(&list, TEST_VAL);

    EXPECT_EQ(pushRetVal, E_OK);
}

TEST(List_PushBack_Tests, pushCheckValWhenNull) {
    CheckT pushRetVal = E_NOT_OK;
    const int TEST_VAL = 7;

    pushRetVal = push_back(NULL, TEST_VAL);

    EXPECT_EQ(pushRetVal, E_NOT_OK);
}

TEST(List_PushBack_Tests, pushMultipleVals) {
    const int WANTED_SIZE = 3;

    ListT list;

    (void)init(&list);

    for(int i = 0; i < WANTED_SIZE; i++) {
        (void)push_back(&list, i);
    }

    NodeT* current = list.root;
    for(int i = 0; i < WANTED_SIZE; i++) {
        EXPECT_EQ(current->item, i);
        current = current->next;
    }
}

TEST(List_PushBack_Tests, checkSizeAfterPush) {
    const size_t WANTED_SIZE = 5;

    ListT list;

    (void)init(&list);

    for(int i = 0; i < WANTED_SIZE; i++) {
        (void)push_back(&list, i);
    }

    EXPECT_EQ(list.size, WANTED_SIZE);
}
