#include "gtest/gtest.h"

extern "C" {
#include "list.h"
}

TEST(List_Clear_Tests, clearCheckVal) {
    CheckT clearRetVal = E_NOT_OK;

    ListT list;

    (void)init(&list);

    for(int i = 0; i < 3; i++) {
        push_back(&list, i);
    }

    clearRetVal = clear(&list);

    EXPECT_EQ(clearRetVal, E_OK);
}

TEST(List_Clear_Tests, clearCheckValWhenNull) {
    CheckT clearRetVal = E_NOT_OK;

    clearRetVal = clear(NO_MEM_ALLOCATED);

    EXPECT_EQ(clearRetVal, E_NOT_OK);
}

TEST(List_Clear_Tests, clearCheckValWhenNoNodes) {
    CheckT clearRetVal = E_NOT_OK;

    ListT list;
    (void)init(&list);

    clearRetVal = clear(NO_MEM_ALLOCATED);

    EXPECT_EQ(clearRetVal, E_NOT_OK);
}

TEST(List_Clear_Tests, sizeCheckAfterClear) {
    const size_t SIZE_AFTER_INIT = 3;
    const size_t SIZE_AFTER_CLEAR = 0u;
    CheckT clearRetVal = E_NOT_OK;

    ListT list;
    (void)init(&list);
    
    for(int i = 0; i < 3; i++) {
        push_back(&list, i);
    }

    EXPECT_EQ(list.size, SIZE_AFTER_INIT);

    (void)clear(&list);

    EXPECT_EQ(list.size, SIZE_AFTER_CLEAR);
}
