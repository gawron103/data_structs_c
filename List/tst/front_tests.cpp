#include "gtest/gtest.h"

extern "C" {
#include "list.h"
}

TEST(List_Front_Tests, checkFrontAfterInit) {
    ItemT* item = NO_MEM_ALLOCATED;

    ListT list;

    (void)init(&list);

    item = front(&list);

    EXPECT_TRUE(item == NO_ITEM);
}

TEST(List_Front_Tests, checkFrontAfterDeInit) {
    ItemT* item = NO_MEM_ALLOCATED;

    ListT list;

    (void)init(&list);
    (void)deinit(&list);

    item = front(&list);

    EXPECT_TRUE(item == NO_ITEM);
}

TEST(List_Front_Tests, checkFrontAfterPushBack) {
    ItemT* item = NO_MEM_ALLOCATED;

    ListT list;

    (void)init(&list);

    for(int i = 0; i < 3; i++) {
        (void)push_back(&list, i);
    }

    item = front(&list);

    EXPECT_TRUE(item != NO_ITEM);
    EXPECT_EQ(*item, 0);
}

TEST(List_Front_Tests, checkFrontAfterPushFront) {
    ItemT* item = NO_MEM_ALLOCATED;

    ListT list;

    (void)init(&list);

    for(int i = 0; i < 3; i++) {
        (void)push_front(&list, i);
    }

    item = front(&list);

    EXPECT_TRUE(item != NO_ITEM);
    EXPECT_EQ(*item, 2);
}

TEST(List_Front_Tests, checkFrontAfterClear) {
    ItemT* item = NO_MEM_ALLOCATED;

    ListT list;

    (void)init(&list);

    for(int i = 0; i < 10; i++) {
        (void)push_front(&list, i);
    }

    (void)clear(&list);

    item = front(&list);

    EXPECT_TRUE(item == NO_ITEM);
}

TEST(List_Front_Tests, checkIfItemChanged) {
    ItemT* item = NO_MEM_ALLOCATED;

    ListT list;

    (void)init(&list);

    for(int i = 0; i < 4; i++) {
        (void)push_front(&list, i);
    }

    item = front(&list);

    *item = 123;

    item = front(&list);

    EXPECT_TRUE(item != NO_ITEM);
    EXPECT_EQ(*item, 123);
}

TEST(List_Front_Tests, checkFrontWhenNull) {
    ItemT* item = NO_MEM_ALLOCATED;

    item = front(NO_MEM_ALLOCATED);

    EXPECT_TRUE(item == NO_ITEM);
}
