#include "gtest/gtest.h"

#ifdef __cplusplus
extern "C" {
#include "list.h"
}
#else
#include "list.h"
#endif

TEST(List_Destructor_Tests, deinitCheckVal) {
    CheckT deinit_retVal = E_NOT_OK;

    ListT list;

    (void)init(&list);
    deinit_retVal = deinit(&list);

    EXPECT_EQ(deinit_retVal, E_OK);
}

TEST(List_Destructor_Tests, structAfterDeInit) {
    ListT list;

    (void)init(&list);
    (void)deinit(&list);

    EXPECT_TRUE(list.root == NO_MEM_ALLOCATED);
    EXPECT_EQ(list.size, SIZE_INVALID_VAL);
}
