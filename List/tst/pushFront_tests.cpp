#include "gtest/gtest.h"

#ifdef __cplusplus
extern "C" {
#include "list.h"
}
#else
#include "list.h"
#endif

TEST(List_PushFront_Tests, pushCheckVal) {
    CheckT push_retVal = E_NOT_OK;
    int TEST_PUSH_VAL = 17;

    ListT list;

    (void)init(&list);

    push_retVal = push_front(&list, TEST_PUSH_VAL);

    EXPECT_EQ(push_retVal, E_OK);
}

TEST(List_PushFront_Tests, pushMultipleVals) {
    const size_t WANTED_SIZE = 17u;

    ListT list;

    (void)init(&list);

    for(size_t i = 0u; i < WANTED_SIZE; i++) {
        (void)push_front(&list, ((int)i));
    }

    NodeT* current = list.root;
    int counter = 0;

    while(NO_MEM_ALLOCATED != current) {
        EXPECT_EQ(current->item, (WANTED_SIZE - 1) - counter);
        current = current->next;
        counter++;
    }
    
}

TEST(List_PushFront_Tests, CheckIfNextItemExists) {
    const size_t WANTED_SIZE = 3u;

    ListT list;

    (void)init(&list);

    for(size_t i = 0u; i < WANTED_SIZE; i++) {
        (void)push_front(&list, ((int)i));
    }

    NodeT* current = list.root;
    size_t nodeCounter = 0u;

    do{
        if(NO_MEM_ALLOCATED != current) {
            nodeCounter++;
        }

        current = current->next;
    }while(NO_MEM_ALLOCATED != current);

    EXPECT_EQ(nodeCounter, WANTED_SIZE);
}

TEST(List_PushFront_Tests, CheckIfPrevItemExists) {
    const size_t WANTED_SIZE = 3u;

    ListT list;

    (void)init(&list);

    for(size_t i = 0u; i < WANTED_SIZE; i++) {
        (void)push_front(&list, ((int)i));
    }

    NodeT* current = list.root;
    size_t nodeCounter = 0u;

    do{
        if(NO_ITEM != current) {
            nodeCounter++;
        }

        current = current->next;
    } while(NO_MEM_ALLOCATED != current);

    EXPECT_EQ(nodeCounter, WANTED_SIZE);
}

TEST(List_PushFront_Tests, checkSizeAfterPush) {
    const size_t WANTED_SIZE = 5;

    ListT list;

    (void)init(&list);

    for(int i = 0; i < WANTED_SIZE; i++) {
        (void)push_front(&list, i);
    }

    EXPECT_EQ(list.size, WANTED_SIZE);
}
