#include "gtest/gtest.h"

extern "C" {
#include "list.h"
}

TEST(List_IsEmpty_Tests, checkIfEmptyWhenNull) {
    CheckT isEmpty = E_NOT_OK;

    isEmpty = is_empty(NO_MEM_ALLOCATED);

    EXPECT_EQ(isEmpty, EMPTY);
}

TEST(List_IsEmpty_Tests, checkIfEmptyAfterInit) {
    CheckT isEmpty = E_NOT_OK;

    ListT list;

    (void)init(&list);

    isEmpty = is_empty(&list);

    EXPECT_EQ(isEmpty, EMPTY);
}

TEST(List_IsEmpty_Tests, checkIfEmptyAfterPushBack) {
    CheckT isEmpty = E_NOT_OK;

    ListT list;

    (void)init(&list);

    for(int i = 0; i < 5; i++) {
        push_back(&list, i);
    }

    isEmpty = is_empty(&list);

    EXPECT_EQ(isEmpty, NOT_EMPTY);
}

TEST(List_IsEmpty_Tests, checkIfEmptyAfterPushFront) {
    CheckT isEmpty = E_NOT_OK;

    ListT list;

    (void)init(&list);

    for(int i = 0; i < 5; i++) {
        push_front(&list, i);
    }

    isEmpty = is_empty(&list);

    EXPECT_EQ(isEmpty, NOT_EMPTY);
}

TEST(List_IsEmpty_Tests, checkIfEmptyAfterDeinit) {
    CheckT isEmpty = E_NOT_OK;

    ListT list;

    (void)init(&list);
    (void)deinit(&list);

    isEmpty = is_empty(&list);

    EXPECT_EQ(isEmpty, EMPTY);
}

TEST(List_IsEmpty_Tests, checkIfEmptyAfterClear) {
    CheckT isEmpty = E_NOT_OK;

    ListT list;

    (void)init(&list);
    
    for(int i = 0; i < 5; i++) {
        push_front(&list, i);
    }

    (void)clear(&list);

    isEmpty = is_empty(&list);

    EXPECT_EQ(isEmpty, EMPTY);
}