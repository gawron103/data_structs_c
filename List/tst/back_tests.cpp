#include "gtest/gtest.h"

extern "C" {
#include "list.h"
}

TEST(List_Back_Tests, checkBackAfterInit) {
    ItemT* item = NO_MEM_ALLOCATED;

    ListT list;

    (void)init(&list);

    item = back(&list);

    EXPECT_TRUE(item == NO_ITEM);
}

TEST(List_Back_Tests, checkBackAfterDeInit) {
    ItemT* item = NO_MEM_ALLOCATED;

    ListT list;

    (void)init(&list);
    (void)deinit(&list);

    item = back(&list);

    EXPECT_TRUE(item == NO_ITEM);
}

TEST(List_Back_Tests, checkBackAfterPushBack) {
    ItemT* item = NO_MEM_ALLOCATED;

    ListT list;

    (void)init(&list);

    for(int i = 0; i < 3; i++) {
        (void)push_back(&list, i);
    }

    item = back(&list);

    EXPECT_TRUE(item != NO_ITEM);
    EXPECT_EQ(*item, 2);
}

TEST(List_Back_Tests, checkBackAfterPushFront) {
    ItemT* item = NO_MEM_ALLOCATED;

    ListT list;

    (void)init(&list);

    for(int i = 0; i < 3; i++) {
        (void)push_front(&list, i);
    }

    item = back(&list);

    EXPECT_TRUE(item != NO_ITEM);
    EXPECT_EQ(*item, 0);
}

TEST(List_Back_Tests, checkBackAfterClear) {
    ItemT* item = NO_MEM_ALLOCATED;

    ListT list;

    (void)init(&list);

    for(int i = 0; i < 10; i++) {
        (void)push_front(&list, i);
    }

    (void)clear(&list);

    item = back(&list);

    EXPECT_TRUE(item == NO_ITEM);
}

TEST(List_Back_Tests, checkIfItemChanged) {
    ItemT* item = NO_MEM_ALLOCATED;

    ListT list;

    (void)init(&list);

    for(int i = 0; i < 4; i++) {
        (void)push_front(&list, i);
    }

    item = back(&list);

    *item = 123;

    item = back(&list);

    EXPECT_TRUE(item != NO_ITEM);
    EXPECT_EQ(*item, 123);
}

TEST(List_Back_Tests, checkBackWhenNull) {
    ItemT* item = NO_MEM_ALLOCATED;

    item = back(NO_MEM_ALLOCATED);

    EXPECT_TRUE(item == NO_ITEM);
}

TEST(List_Back_Tests, checkBackAfterMixedPushes) {
    ItemT* item = NO_MEM_ALLOCATED;

    ListT list;

    (void)init(&list);

    (void)push_front(&list, 7);
    (void)push_back(&list, 9);

    for(int i = 0; i < 6; i++) {
        (void)push_back(&list, i);
    }

    for(int i = 10; i < 13; i++) {
        (void)push_front(&list, i);
    }

    (void)push_back(&list, 9);
    (void)push_front(&list, 7);
    
    item = back(&list);

    EXPECT_EQ(*item, 9);
}
