//==========================================================//
// FILE ENCODING: UTF-8                                     //
//==========================================================//

//==========================================================//
// INCLUDES SECTION START                                   //
//==========================================================//
#include <stdlib.h>
#include <stdio.h>

#include "list.h"
//==========================================================//
// INCLUDES SECTION END                                     //
//==========================================================//

//==========================================================//
// FUNCTIONS IMPLEMENTATIONS SECTION START                  //
//==========================================================//

//==========================================================//
// DESCRIPTION: Function for aligning the desired memory for
//              list and for setting it's attributes to 
//              default ones.
//
// PARAMETERS: list: struct representing list data structrue.
//
// RETURN VALUE: E_OK: succesfull initialization.
//               E_NOT_OK: unsuccessful initialization.
//==========================================================//
CheckT init(ListT* const list) {
    CheckT status = E_NOT_OK;

    if(NO_MEM_ALLOCATED != list) {
        list->size = SIZE_INIT_VAL;
        list->root = NO_ITEM;
        list->tail = NO_ITEM;

        status = E_OK;
    }

    return status;
}

//==========================================================//
// DESCRIPTION: Function for dealocating space owned by
//              list and setting it's attributes to invalid.
//
// PARAMETERS: list: struct representing list data structrue.
//
// RETURN VALUE: E_OK: succesfull initialization.
//               E_NOT_OK: unsuccessful initialization.
//==========================================================//
CheckT deinit(ListT* const list) {
    CheckT status = E_NOT_OK;

    if(NO_MEM_ALLOCATED != list) {
        NodeT* current = list->root;

        while(NO_MEM_ALLOCATED != current) {
            NodeT* tmp = current->next;

            free(current);

            current = tmp;
        }

        // Set list attributes to invalid
        list->root = NO_MEM_ALLOCATED;
        list->tail = NO_MEM_ALLOCATED;
        list->size = SIZE_INVALID_VAL;

        status = E_OK;
    }

    return status;
}

//==========================================================//
// DESCRIPTION: 
//
// PARAMETERS: 
//
// RETURN VALUE: 
//==========================================================//
CheckT insert(ListT* const list, const int pos, const int val) {
    CheckT status = E_NOT_OK;

    return status;
}

//==========================================================//
// DESCRIPTION: Function for adding new item to list front.
//
// PARAMETERS: list: struct representing list data structrue.
//
// RETURN VALUE: E_OK: the item was added to the list.
//               E_NOT_OK: the item wasn't added to
//                         the list.
//==========================================================//
CheckT push_front(ListT* const list, const int val) {
    CheckT status = E_NOT_OK;

    if(NO_MEM_ALLOCATED != list) {
        // Create new node
        NodeT* newRoot = (NodeT*)malloc(sizeof(NodeT));
        newRoot->item = val;
        newRoot->prev = NO_ITEM;

        // No root in list
        if(NO_MEM_ALLOCATED == list->root) {
            list->root = newRoot;
            list->tail = newRoot;
            newRoot->next = NO_ITEM;
        }
        // Root already exists 
        else {
            NodeT* oldRoot = list->root;
            list->root = newRoot;
            list->root->next = oldRoot;
            oldRoot->prev = list->root;
        }

        list->size++;

        status = E_OK;
    }

    return status;
}

//==========================================================//
// DESCRIPTION: Function for adding item to the back of
//              the list.
//
// PARAMETERS: list: struct representing list data structrue.
//
// RETURN VALUE: E_OK: the item was added to the list.
//               E_NOT_OK: the item wasn't added to
//                         the list.
//==========================================================//
CheckT push_back(ListT* const list, const int val) {
    CheckT status = E_NOT_OK;

    if(NO_MEM_ALLOCATED != list) {
        // Create new node
        NodeT* node = (NodeT*)malloc(sizeof(NodeT));
        node->item = val;
        node->next = NO_ITEM;

        // If no root, make new node root
        if(NO_MEM_ALLOCATED == list->root) {
            list->root = node;
            list->tail = node;
            node->prev = NO_ITEM;
        }
        else {
            NodeT* current = list->root;

            // Iterate to the last item
            while(NO_ITEM != current->next) {
                current = current->next;
            }

            current->next = node;
            node->prev = current;

            list->tail = node;
        }

        list->size++;

        status = E_OK;
    }

    return status;
}

//==========================================================//
// DESCRIPTION: 
//
// PARAMETERS: 
//
// RETURN VALUE: 
//==========================================================//
CheckT pop_front(ListT* const list) {
    CheckT status = E_NOT_OK;

    return status;
}

//==========================================================//
// DESCRIPTION: 
//
// PARAMETERS: 
//
// RETURN VALUE: 
//==========================================================//
CheckT pop_back(ListT* const list) {
    CheckT status = E_NOT_OK;

    return status;
}

//==========================================================//
// DESCRIPTION: 
//
// PARAMETERS: 
//
// RETURN VALUE: 
//==========================================================//
CheckT erase(ListT* const list, const int pos) {
    CheckT status = E_NOT_OK;

    return status;
}

//==========================================================//
// DESCRIPTION: Function that erases all elements from list.
//
// PARAMETERS: list: struct representing list data structrue.
//
// RETURN VALUE: E_OK: list has been cleared.
//               E_NOT_OK: list hasn't been cleared.
//==========================================================//
CheckT clear(ListT* const list) {
    CheckT status = E_NOT_OK;

    if(NO_MEM_ALLOCATED != list) {
        NodeT* current = list->root; 
        NodeT* next = NO_ITEM;

        while(current != NO_ITEM) {
            next = current->next;

            current->next = NO_ITEM;
            current->prev = NO_ITEM;

            free(current);

            current = next;
        }

        list->root = NO_ITEM;
        list->tail = NO_ITEM;
        list->size = SIZE_INIT_VAL;

        status = E_OK;
    }

    return status;
}

//==========================================================//
// DESCRIPTION: Function that checks if list is empty.
//
// PARAMETERS: list: struct representing list data structrue.
//
// RETURN VALUE: EMPTY: list has no members.
//               NOT_EMPTY: list has at least one member.
//==========================================================//
CheckT is_empty(const ListT* const list) {
    CheckT status = EMPTY;

    if(NO_MEM_ALLOCATED != list) {
        if(list->size > SIZE_INIT_VAL) {
            status = NOT_EMPTY;
        }
    }

    return status;
}

//==========================================================//
// DESCRIPTION: 
//
// PARAMETERS: 
//
// RETURN VALUE: 
//==========================================================//
CheckT reverse(ListT* const list) {
    CheckT status = E_NOT_OK;

    return status;
}

//==========================================================//
// DESCRIPTION: Function that returns address to the first
//              item in the list.
//
// PARAMETERS: list: struct representing list data structrue.
//
// RETURN VALUE: NO_ITEM: when no memory has been allocated
//                        for list.
//               ItemT: first item in the list.
//==========================================================//
ItemT* front(ListT* const list) {
    ItemT* status = NO_ITEM;

    if(NO_MEM_ALLOCATED != list) {
        status = &(list->root->item);
    }

    return status;
}

//==========================================================//
// DESCRIPTION: Function that returns address to the last
//              item in the list.
//
// PARAMETERS: list: struct representing list data structrue.
//
// RETURN VALUE: NO_ITEM: when no memory has been allocated
//                        for list.
//               ItemT: last item in the list.
//==========================================================//
ItemT* back(ListT* const list) {
    ItemT* status = NO_ITEM;

    if(NO_MEM_ALLOCATED != list) {
        status = &(list->tail->item);
    }

    return status;
}

//==========================================================//
// DESCRIPTION: Function that returns list current size.
//
// PARAMETERS: list: struct representing list data structrue.
//
// RETURN VALUE: SIZE_INVALID_VAL: when list is not
//                                 initialized.
//               status: list current size.
//==========================================================//
SizeT size(const ListT* const list) {
    SizeT status = SIZE_INVALID_VAL;

    if(NO_MEM_ALLOCATED != list) {
        status = list->size;
    }

    return status;
}

//==========================================================//
// FUNCTIONS IMPLEMENTATIONS SECTION END                    //
//==========================================================//

//==========================================================//
// EOF
//==========================================================//
