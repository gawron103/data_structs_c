//==========================================================//
// FILE ENCODING: UTF-8                                     //
//==========================================================//

#ifndef LIST_H
#define LIST_H

//==========================================================//
// INCLUDES SECTION START                                   //
//==========================================================//
#include <stddef.h>
#include <stdbool.h>

#include "node.h"
//==========================================================//
// INCLUDES SECTION END                                     //
//==========================================================//

//==========================================================//
// TYPES SECTION START                                      //
//==========================================================//
typedef bool CheckT;
typedef signed int SizeT;

typedef struct {
    NodeT* root;
    NodeT* tail;
    SizeT size;
} ListT;
//==========================================================//
// TYPES SECTION END                                        //
//==========================================================//

//==========================================================//
// CONSTANT SECTION START                                   //
//==========================================================//
#define E_NOT_OK  ((CheckT) 0u)
#define E_OK      ((CheckT) 1u)

#define SIZE_INVALID_VAL ((SizeT) -1)

#define SIZE_INIT_VAL ((SizeT) 0)

#define EMPTY     ((CheckT) 0u)
#define NOT_EMPTY ((CheckT) 1u)

#define NO_MEM_ALLOCATED (NULL)

#define NO_ITEM   (NULL)
//==========================================================//
// CONSTANT SECTION END                                     //
//==========================================================//

//==========================================================//
// FUNCTION PROTOTYPES SECTION START                        //
//==========================================================//
CheckT init(ListT* const list);
CheckT deinit(ListT* const list);
CheckT insert(ListT* const list, const int pos, const int val);
CheckT push_front(ListT* const list, const int val);
CheckT push_back(ListT* const list, const int val);
CheckT pop_front(ListT* const list);
CheckT pop_back(ListT* const list);
CheckT erase(ListT* const list, const int pos);
CheckT clear(ListT* const list);
CheckT is_empty(const ListT* const list);
CheckT reverse(ListT* const list);
ItemT* front(ListT* const list);
ItemT* back(ListT* const list);
SizeT size(const ListT* const list);
//==========================================================//
// FUNCTION PROTOTYPES SECTION END                          //
//==========================================================//

#endif // LIST_H

//==========================================================//
// EOF
//==========================================================//
