//==========================================================//
// FILE ENCODING: UTF-8                                     //
//==========================================================//

#ifndef QUEUE_H
#define QUEUE_H

//==========================================================//
// INCLUDES SECTION START                                   //
//==========================================================//
#include <stdbool.h>
#include <limits.h>
//==========================================================//
// INCLUDES SECTION END                                     //
//==========================================================//

//==========================================================//
// TYPES SECTION START                                      //
//==========================================================//
typedef bool CheckT;
typedef signed int PositionT;
typedef signed int CapacityT;
typedef signed int SizeT;
typedef int* ItemT;

typedef struct {
    ItemT data;
    PositionT back;
    SizeT size;
    CapacityT capacity;
} QueueT;
//==========================================================//
// TYPES SECTION END                                        //
//==========================================================//

//==========================================================//
// CONSTANT SECTION START                                   //
//==========================================================//
#define E_NOT_OK             ((CheckT) 0u)
#define E_OK                 ((CheckT) 1u)

#define NOT_EMPTY            ((CheckT) 0u)
#define EMPTY                ((CheckT) 1u)

#define NOT_FULL             ((CheckT) 0u)
#define FULL                 ((CheckT) 1u)

#define BACK_INIT_VAL        ((PositionT) -1)
#define SIZE_INIT_VAL        ((SizeT) 0)

#define BACK_INVALID_VAL     ((PositionT) INT_MIN)
#define SIZE_INVALID_VAL     ((SizeT) -1)
#define CAPACITY_INVALID_VAL ((CapacityT) -1)

#define FIRST_ITEM           ((PositionT) 0)

#define NO_MEM_ALLOCATED     (NULL)

#define NO_ITEM              (NULL)
//==========================================================//
// CONSTANT SECTION END                                     //
//==========================================================//

//==========================================================//
// FUNCTION PROTOTYPES SECTION START                        //
//==========================================================//
CheckT init(QueueT* const queue, const SizeT size);
CheckT deinit(QueueT* const queue);
CheckT push(QueueT* const queue, const int val);
CheckT pop(QueueT* const queue);
CheckT is_empty(const QueueT* const queue);
CheckT is_full(const QueueT* const queue);
ItemT front(const QueueT* const queue);
ItemT back(const QueueT* const queue);
//==========================================================//
// FUNCTION PROTOTYPES SECTION END                          //
//==========================================================//

#endif // QUEUE_H

//==========================================================//
// EOF
//==========================================================//
