//==========================================================//
// FILE ENCODING: UTF-8                                     //
//==========================================================//

//==========================================================//
// INCLUDES SECTION START                                   //
//==========================================================//
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "queue.h"
//==========================================================//
// INCLUDES SECTION END                                     //
//==========================================================//

//==========================================================//
// FUNCTIONS IMPLEMENTATIONS SECTION START                  //
//==========================================================//

//==========================================================//
// DESCRIPTION: Function for aligning the desired memory for
//              queue and for setting it's attributes to 
//              default ones.
//
// PARAMETERS: queue: struct representing queue 
//                    data structure.
//             size: desired queue size.
//
// RETURN VALUE: E_OK: succesfull initialization.
//               E_NOT_OK: unsuccessful initialization.
//==========================================================//
CheckT init(QueueT* const queue, const SizeT size) {
    CheckT status = E_NOT_OK;

    if(NO_MEM_ALLOCATED != queue) {
        queue->capacity = size;

        queue->data = (int*)malloc(sizeof(int) * queue->capacity);

        // Check if memory allocated successfully
        if(queue->data == NO_MEM_ALLOCATED) {
            fprintf(stderr, "Malloc failed in init\n");
        }

        queue->back = BACK_INIT_VAL;
        queue->size = SIZE_INIT_VAL;

        status = E_OK;
    }

    return status;
}

//==========================================================//
// DESCRIPTION: Function for dealocating space owned by
//              queue and setting it's attributes to invalid.
//
// PARAMETERS: queue: struct representing queue 
//                    data structure.
//
// RETURN VALUE: E_OK: memory has been released.
//               E_NOT_OK: memory haven't been released.
//==========================================================//
CheckT deinit(QueueT* const queue) {
    CheckT status = E_NOT_OK;

    if(NO_MEM_ALLOCATED != queue) {
        free(queue->data);

        // Set queue attributes to invalid
        queue->data = NO_MEM_ALLOCATED;
        queue->back = BACK_INVALID_VAL;
        queue->size = SIZE_INVALID_VAL;
        queue->capacity = CAPACITY_INVALID_VAL;

        status = E_OK;
    }

    return status;
}

//==========================================================//
// DESCRIPTION: Function for adding new items to queue.
//
// PARAMETERS: queue: struct representing queue 
//                    data structure.
//             val: new storage item.
//
// RETURN VALUE: E_OK: the item was added to queue.
//               E_NOT_OK: the item wasn't added to
//                         the queue.
//==========================================================//
CheckT push(QueueT* const queue, const int val) {
    CheckT status = E_NOT_OK;

    if(NO_MEM_ALLOCATED != queue) {
        const CheckT IS_FULL = is_full(queue);

        // Check if queue is not already full
        if(FULL != IS_FULL) {
            queue->back++;
            queue->data[queue->back] = val;
            queue->size++;
            status = E_OK;
        }
    }

    return status;
}

//==========================================================//
// DESCRIPTION: Function that removes the first item
//              from the queue
//
// PARAMETERS: queue: struct representing queue 
//                    data structure.
//
// RETURN VALUE: E_OK: item has been removed
//                     from queue.
//               E_NOT_OK: item hasn't been removed
//                         from queue.
//==========================================================//
CheckT pop(QueueT* const queue) {
    CheckT status = E_NOT_OK;

    if(NO_MEM_ALLOCATED != queue) {
        const CheckT IS_EMPTY = is_empty(queue);

        // If queue is not empty,
        if(EMPTY != IS_EMPTY) {
            // Move data by one and decrease back
            (void)memmove(queue->data, &(queue->data[1]), sizeof(int) * (queue->capacity - 1));
            queue->back--;
            queue->size--;
            status = E_OK;
        }
    }

    return status;
}

//==========================================================//
// DESCRIPTION: Function that specifies if queue is empty.
//
// PARAMETERS: queue: struct representing queue 
//                    data structure.
//
// RETURN VALUE: EMPTY: when queue has no items.
//               NOT_EMPTY: when queue has at least one item.
//==========================================================//
CheckT is_empty(const QueueT* const queue) {
    CheckT status = EMPTY;

    if(NO_MEM_ALLOCATED != queue) {
        status = BACK_INIT_VAL < queue->back ? NOT_EMPTY : EMPTY;
    }

    return status;
}

//==========================================================//
// DESCRIPTION: Function that returns if queue is already
//              full.
//
// PARAMETERS: queue: struct representing queue 
//                    data structure.
//
// RETURN VALUE: FULL: when queue is already full.
//               NOT_FULL: when queue still has some space.
//==========================================================//
CheckT is_full(const QueueT* const queue) {
    CheckT status = NOT_FULL;

    if(NO_MEM_ALLOCATED != queue) {
        status = queue->size == queue->capacity ? FULL : NOT_FULL;
    }

    return status;
}

//==========================================================//
// DESCRIPTION: Function that returns pointer to the first 
//              item in queue.
//
// PARAMETERS: queue: struct representing queue 
//                    data structure.
//
// RETURN VALUE: ItemT: pointer to the first queue item.
//==========================================================//
ItemT front(const QueueT* const queue) {
    ItemT item = NO_ITEM;

    if(NO_MEM_ALLOCATED != queue) {
        const CheckT IS_EMPTY = is_empty(queue);

        // If queue is not empty, return pointer to first item
        if(EMPTY != IS_EMPTY) {
            item = &(queue->data[FIRST_ITEM]);
        }
    }

    return item;
}

//==========================================================//
// DESCRIPTION: Function that returns pointer to the last 
//              item in queue.
//
// PARAMETERS: queue: struct representing queue 
//                    data structure.
//
// RETURN VALUE: ItemT: pointer to the last queue item.
//==========================================================//
ItemT back(const QueueT* const queue) {
    ItemT item = NO_ITEM;

    if(NO_MEM_ALLOCATED != queue) {
        const CheckT IS_EMPTY = is_empty(queue);

        // If queue is not empty, return pointer to last item
        if(EMPTY != IS_EMPTY) {
            item = &(queue->data[queue->back]);
        }
    }
    
    return item;
}

//==========================================================//
// FUNCTIONS IMPLEMENTATIONS SECTION END                    //
//==========================================================//

//==========================================================//
// EOF
//==========================================================//
