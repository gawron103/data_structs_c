## Queue
![alt text](https://mbcmc27n7s3en3e22s6bb816-wpengine.netdna-ssl.com/wp-content/uploads/2017/01/Queue_queuing_upirloko89_i_opt-1.jpg "Queue")

[Queue](https://en.wikipedia.org/wiki/Queue_(abstract_data_type)) data structure implementation.

***

## Technical informations
**Build with**
- [CMake](https://cmake.org/)

**Written in**
- [C](https://en.cppreference.com/w/c/language)

**Tested with**
- [GTest](https://github.com/google/googletest)

***

## Basic usage
```C
#include <stdio.h>

#include "queue.h"

int main() {
    // Create queue
    QueueT queue;

    // Initialize queue
    init(&queue, 10);

    // Add new item
    push(&queue, 1);

    // Get first item
    int* first = front(&queue);
    
    // Get last item
    int* last = back(&queue);

    // Remove item
    pop(&queue);

    // Deinit queue
    deinit(&queue);
}
```

***

## How to use
Copy files from **/src** dir and include header file.

***

## Tests
Every function has been tested using [GTest](https://github.com/google/googletest). There are currently 35 tests in total.

***

## To do
- Create better tests or refactor existing.
- Change CMake script.