cmake_minimum_required (VERSION 3.1)

# Download and install GTest
include(ExternalProject)
ExternalProject_Add(gtest
  URL https://github.com/google/googletest/archive/release-1.8.0.zip

  PREFIX ${PROJECT_SOURCE_DIR}/lib/gtest
  INSTALL_COMMAND ""
)