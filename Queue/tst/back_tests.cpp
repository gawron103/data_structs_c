#include "gtest/gtest.h"

#ifdef __cplusplus
extern "C" {
#include "queue.h"
}
#else
#include "queue.h"
#endif

TEST(Queue_Back_Tests, backAfterInit) {
    const size_t WANTED_SIZE = 5u;
    ItemT ItemT = NULL;

    QueueT queue;

    (void)init(&queue, WANTED_SIZE);

    ItemT = back(&queue);

    EXPECT_TRUE(ItemT == NULL);
}

TEST(Queue_Back_Tests, backAterDeinit) {
    const size_t WANTED_SIZE = 5u;
    ItemT ItemT = NULL;

    QueueT queue;

    (void)init(&queue, WANTED_SIZE);
    (void)deinit(&queue);
    
    ItemT = back(&queue);

    EXPECT_TRUE(ItemT == NULL);
}

TEST(Queue_Back_Tests, backAfterPush) {
    const size_t WANTED_SIZE = 5u;
    ItemT ItemT = NULL;
    int TEST_PUSH_VAL = 3;

    QueueT queue;

    (void)init(&queue, WANTED_SIZE);

    (void)push(&queue, TEST_PUSH_VAL);

    ItemT = back(&queue);

    EXPECT_TRUE(ItemT != NULL);
}

TEST(Queue_Back_Tests,backAfterPop) {
    const size_t WANTED_SIZE = 5u;
    ItemT ItemT = NULL;
    int TEST_PUSH_VAL = 3;

    QueueT queue;

    (void)init(&queue, WANTED_SIZE);

    (void)push(&queue, TEST_PUSH_VAL);
    (void)pop(&queue);

    ItemT = back(&queue);

    EXPECT_TRUE(ItemT == NULL);
}

TEST(Queue_Back_Tests, valueChanged) {
    const size_t WANTED_SIZE = 5u;
    ItemT item = NULL;
    int TEST_PUSH_VAL = 3;

    QueueT queue;

    (void)init(&queue, WANTED_SIZE);

    (void)push(&queue, TEST_PUSH_VAL);

    item = back(&queue);
    *item = 5;

    EXPECT_EQ(queue.data[queue.back], 5);
}
