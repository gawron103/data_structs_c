#include "gtest/gtest.h"

#ifdef __cplusplus
extern "C" {
#include "queue.h"
}
#else
#include "queue.h"
#endif

//==========================================================//

//==========================================================//
// Check return value of pop function
//==========================================================//
TEST(Queue_Pop_Tests, popCheckVal) {
    const size_t WANTED_SIZE = 20u;
    CheckT pop_retVal = E_NOT_OK;

    QueueT queue;

    (void)init(&queue, WANTED_SIZE);

    for(size_t i = 0u; i < WANTED_SIZE; i++) {
        (void)push(&queue, (int)i);
    }

    pop_retVal = pop(&queue);

    EXPECT_EQ(pop_retVal, E_OK);
}

TEST(Queue_Pop_Tests, tooMuchPopCheckVal) {
    const size_t WANTED_SIZE = 20u;
    CheckT pop_retVal = E_NOT_OK;

    QueueT queue;

    (void)init(&queue, WANTED_SIZE);

    for(size_t i = 0u; i < WANTED_SIZE; i++) {
        (void)push(&queue, (int)i);
    }

    for(size_t i = 0u; i < WANTED_SIZE; i++) {
        (void)pop(&queue);
    }

    pop_retVal = pop(&queue);

    EXPECT_EQ(pop_retVal, E_NOT_OK);
}

TEST(Queue_Pop_Tests, checkIfDataMoved) {
    const size_t WANTED_SIZE = 5u;

    QueueT queue;

    (void)init(&queue, WANTED_SIZE);

    for(size_t i = 0u; i < WANTED_SIZE; i++) {
        (void)push(&queue, (int)i);
    }

    (void)pop(&queue);

    EXPECT_EQ(queue.data[0], 1);
    EXPECT_EQ(queue.data[1], 2);
    EXPECT_EQ(queue.data[2], 3);
    EXPECT_EQ(queue.data[3], 4);
}

TEST(Queue_Pop_Tests, checkBackPositionT1) {
    const size_t WANTED_SIZE = 5u;

    QueueT queue;

    (void)init(&queue, WANTED_SIZE);

    for(size_t i = 0u; i < WANTED_SIZE; i++) {
        (void)push(&queue, (int)i);
    }

    (void)pop(&queue);

    EXPECT_EQ(queue.back, queue.size - 1);
    EXPECT_EQ(queue.data[queue.back], 4);
}

TEST(Queue_Pop_Tests, checkBackPositionT2) {
    const size_t WANTED_SIZE = 5u;

    QueueT queue;

    (void)init(&queue, WANTED_SIZE);

    for(size_t i = 0u; i < WANTED_SIZE; i++) {
        (void)push(&queue, (int)i);
    }

    for(size_t i = 0u; i < WANTED_SIZE; i++) {
        (void)pop(&queue);
    }

    EXPECT_EQ(queue.back, -1);
}
