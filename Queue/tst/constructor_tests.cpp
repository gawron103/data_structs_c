#include "gtest/gtest.h"

#ifdef __cplusplus
extern "C" {
#include "queue.h"
}
#else
#include "queue.h"
#endif

TEST(Queue_Constructor_Tests, initCheckVal) {
    const size_t WANTED_SIZE = 10u;
    CheckT init_retVal = E_NOT_OK;

    QueueT queue;

    init_retVal = init(&queue, WANTED_SIZE);

    EXPECT_EQ(init_retVal, E_OK);
}

TEST(Queue_Constructor_Tests, structValuesAfterInit) {
    const size_t WANTED_SIZE = 10u;

    QueueT queue;
    (void)init(&queue, WANTED_SIZE);

    EXPECT_TRUE(queue.data != NULL);
    EXPECT_EQ(queue.back, BACK_INIT_VAL);
    EXPECT_EQ(queue.size, SIZE_INIT_VAL);
    EXPECT_EQ(queue.capacity, WANTED_SIZE);
}

TEST(Queue_Constructor_Tests, structInitWithAssignedVals) {
    const size_t WANTED_SIZE = 10u;

    QueueT queue = {
        .data = NULL,
        .back = 1,
        .size = 6
    };

    (void)init(&queue, WANTED_SIZE);

    EXPECT_TRUE(queue.data != NULL);
    EXPECT_EQ(queue.back, BACK_INIT_VAL);
    EXPECT_EQ(queue.size, SIZE_INIT_VAL);
    EXPECT_EQ(queue.capacity, WANTED_SIZE);
}
