#include "gtest/gtest.h"

#ifdef __cplusplus
extern "C" {
#include "queue.h"
}
#else
#include "queue.h"
#endif

TEST(Queue_Empty_Tests, isEmptyAfterInit) {
    const size_t WANTED_SIZE = 10u;
    bool isEmpty = EMPTY;

    QueueT queue;

    (void)init(&queue, WANTED_SIZE);

    isEmpty = is_empty(&queue);

    EXPECT_EQ(isEmpty, EMPTY);
}

TEST(Queue_Empty_Tests, isEmptyAfterDeinit) {
    const size_t WANTED_SIZE = 10u;
    bool isEmpty = EMPTY;

    QueueT queue;

    (void)init(&queue, WANTED_SIZE);
    (void)deinit(&queue);

    isEmpty = is_empty(&queue);

    EXPECT_EQ(isEmpty, EMPTY);
}

TEST(Queue_Empty_Tests, isEmptyAfterPush) {
    const size_t WANTED_SIZE = 10u;
    bool isEmpty = EMPTY;

    QueueT queue;

    (void)init(&queue, WANTED_SIZE);

    for(size_t i = 0u; i < WANTED_SIZE; i++) {
        (void)push(&queue, (int)i);
    }

    isEmpty = is_empty(&queue);

    EXPECT_EQ(isEmpty, NOT_EMPTY);
}

TEST(Queue_Empty_Tests, isEmptyAfterPopSome) {
    const size_t WANTED_SIZE = 10u;
    bool isEmpty = EMPTY;

    QueueT queue;

    (void)init(&queue, WANTED_SIZE);

    for(size_t i = 0u; i < WANTED_SIZE; i++) {
        (void)push(&queue, (int)i);
    }

    for(size_t i = 0u; i < (WANTED_SIZE / 2); i++) {
        (void)pop(&queue);
    }

    isEmpty = is_empty(&queue);

    EXPECT_EQ(isEmpty, NOT_EMPTY);
}

TEST(Queue_Empty_Tests, isEmptyAfterPopAll) {
    const size_t WANTED_SIZE = 10u;
    bool isEmpty = EMPTY;

    QueueT queue;

    (void)init(&queue, WANTED_SIZE);

    for(size_t i = 0u; i < WANTED_SIZE; i++) {
        (void)push(&queue, (int)i);
    }

    for(size_t i = 0u; i < WANTED_SIZE; i++) {
        (void)pop(&queue);
    }

    isEmpty = is_empty(&queue);

    EXPECT_EQ(isEmpty, EMPTY);
}
