#include "gtest/gtest.h"

#ifdef __cplusplus
extern "C" {
#include "queue.h"
}
#else
#include "queue.h"
#endif

TEST(Queue_Full_Tests, isFullAfterInit) {
    const size_t WANTED_SIZE = 3u;
    bool isFull = NOT_FULL;

    QueueT queue;

    (void)init(&queue, WANTED_SIZE);

    isFull = is_full(&queue);

    EXPECT_EQ(isFull, NOT_FULL);
}

TEST(Queue_Full_Tests, isFullAfterPush) {
    const size_t WANTED_SIZE = 3u;
    bool isFull = NOT_FULL;
    int TEST_PUSH_VAL = 1;

    QueueT queue;

    (void)init(&queue, WANTED_SIZE);
    (void)push(&queue, TEST_PUSH_VAL);

    isFull = is_full(&queue);

    EXPECT_EQ(isFull, NOT_FULL);
}

TEST(Queue_Full_Tests, isFullAfterMaxPushes) {
    const size_t WANTED_SIZE = 3u;
    bool isFull = NOT_FULL;

    QueueT queue;

    (void)init(&queue, WANTED_SIZE);

    for(size_t i = 0u; i < WANTED_SIZE; i++) {
        (void)push(&queue, (int)i);
    }

    isFull = is_full(&queue);

    EXPECT_EQ(isFull, FULL);
}

TEST(Queue_Full_Tests, isFullAfterMaxPushesAndPop) {
    const size_t WANTED_SIZE = 3u;
    bool isFull = NOT_FULL;

    QueueT queue;

    (void)init(&queue, WANTED_SIZE);

    for(size_t i = 0u; i < WANTED_SIZE; i++) {
        (void)push(&queue, (int)i);
    }

    (void)pop(&queue);

    isFull = is_full(&queue);

    EXPECT_EQ(isFull, NOT_FULL);
}
