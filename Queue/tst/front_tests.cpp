#include "gtest/gtest.h"

#ifdef __cplusplus
extern "C" {
#include "queue.h"
}
#else
#include "queue.h"
#endif

TEST(Queue_Front_Tests, frontAfterInit) {
    const size_t WANTED_SIZE = 5u;
    ItemT ItemT = NULL;

    QueueT queue;

    (void)init(&queue, WANTED_SIZE);

    ItemT = front(&queue);

    EXPECT_TRUE(ItemT == NULL);
}

TEST(Queue_Front_Tests, frontAterDeinit) {
    const size_t WANTED_SIZE = 5u;
    ItemT ItemT = NULL;

    QueueT queue;

    (void)init(&queue, WANTED_SIZE);
    (void)deinit(&queue);
    
    ItemT = front(&queue);

    EXPECT_TRUE(ItemT == NULL);
}

TEST(Queue_Front_Tests, frontAfterPush) {
    const size_t WANTED_SIZE = 5u;
    ItemT ItemT = NULL;
    int TEST_PUSH_VAL = 6;

    QueueT queue;

    (void)init(&queue, WANTED_SIZE);

    (void)push(&queue, TEST_PUSH_VAL);

    ItemT = front(&queue);

    EXPECT_TRUE(ItemT != NULL);
}

TEST(Queue_Front_Tests, frontAfterPop) {
    const size_t WANTED_SIZE = 5u;
    ItemT ItemT = NULL;
    int TEST_PUSH_VAL = 6;

    QueueT queue;

    (void)init(&queue, WANTED_SIZE);

    (void)push(&queue, TEST_PUSH_VAL);
    (void)pop(&queue);

    ItemT = front(&queue);

    EXPECT_TRUE(ItemT == NULL);
}

TEST(Queue_Front_Tests, valueChanged) {
    const size_t WANTED_SIZE = 5u;
    ItemT item = NULL;
    int TEST_PUSH_VAL = 3;

    QueueT queue;

    (void)init(&queue, WANTED_SIZE);

    (void)push(&queue, TEST_PUSH_VAL);
    (void)push(&queue, (TEST_PUSH_VAL * TEST_PUSH_VAL));

    item = front(&queue);
    *item = -55;

    EXPECT_EQ(queue.data[0], -55);
}
