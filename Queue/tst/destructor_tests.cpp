#include "gtest/gtest.h"

#ifdef __cplusplus
extern "C" {
#include "queue.h"
}
#else
#include "queue.h"
#endif

TEST(Queue_Destructor_Tests, deinitCheckVal) {
    const size_t WANTED_SIZE = 10u;
    CheckT deinit_retVal = E_NOT_OK;

    QueueT queue;
    (void)init(&queue, WANTED_SIZE);

    deinit_retVal = deinit(&queue);

    EXPECT_EQ(deinit_retVal, E_OK);
}

TEST(Queue_Destructor_Tests, structAfterDeInit) {
    const size_t WANTED_SIZE = 10u;

    QueueT queue;
    (void)init(&queue, WANTED_SIZE);

    (void)deinit(&queue);

    EXPECT_TRUE(queue.data == NULL);
    EXPECT_EQ(queue.back, BACK_INVALID_VAL);
    EXPECT_EQ(queue.size, SIZE_INVALID_VAL);
    EXPECT_EQ(queue.capacity, CAPACITY_INVALID_VAL);
}
