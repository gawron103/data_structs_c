#include "gtest/gtest.h"

#ifdef __cplusplus
extern "C" {
#include "queue.h"
}
#else
#include "queue.h"
#endif

TEST(Queue_Push_Tests, pushCheckVal) {
    const size_t WANTED_SIZE = 17u;
    CheckT push_retVal = E_NOT_OK;
    int TEST_PUSH_VAL = 17;

    QueueT queue;

    (void)init(&queue, WANTED_SIZE);


    push_retVal = push(&queue, TEST_PUSH_VAL);

    EXPECT_EQ(push_retVal, E_OK);
}

TEST(Queue_Push_Tests, pushMultipleVals) {
    const size_t WANTED_SIZE = 17u;

    QueueT queue;

    (void)init(&queue, WANTED_SIZE);

    for(size_t i = 0u; i < WANTED_SIZE; i++) {
        (void)push(&queue, ((int)i));
    }

    for(size_t i = 0u; i < WANTED_SIZE; i++) {
        EXPECT_EQ(i, queue.data[i]);
    }
}

TEST(Queue_Push_Tests, pushTooMuchVals) {
    const size_t WANTED_SIZE = 17u;
    CheckT push_retVal = E_NOT_OK;
    int TEST_PUSH_VAL = 17;

    QueueT queue;

    (void)init(&queue, WANTED_SIZE);

    for(size_t i = 0u; i < WANTED_SIZE; i++) {
        (void)push(&queue, (int)i);
    }

    push_retVal = push(&queue, TEST_PUSH_VAL);

    EXPECT_EQ(push_retVal, E_NOT_OK);
}

TEST(Queue_Push_Tests, checkBackPositionT) {
    const size_t WANTED_SIZE = 17u;
    CheckT push_retVal = E_NOT_OK;

    QueueT queue;

    (void)init(&queue, WANTED_SIZE);

    for(size_t i = 0u; i < WANTED_SIZE; i++) {
        (void)push(&queue, (int)i);
    }

    EXPECT_EQ(queue.back, queue.size - 1);
}

TEST(Queue_Push_Tests, checkPushRetValWhenCapacityTZero) {
    const size_t WANTED_SIZE = 0u;
    CheckT push_retVal = E_NOT_OK;

    QueueT queue;

    (void)init(&queue, WANTED_SIZE);

    push_retVal = push(&queue, 0);

    EXPECT_EQ(push_retVal, E_NOT_OK);
}

TEST(Queue_Push_Tests, checkQueueAttribsAfterPushWhenCapacityTZero) {
    const size_t WANTED_SIZE = 0u;

    QueueT queue;

    (void)init(&queue, WANTED_SIZE);

    (void)push(&queue, 0);

    EXPECT_EQ(queue.back, -1);
    EXPECT_EQ(queue.capacity, 0);
    EXPECT_EQ(queue.size, 0);
}