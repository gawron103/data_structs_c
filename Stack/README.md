## Stack
![alt text](https://sites.sandiego.edu/usd-magazine/files/2017/04/pancakes.jpg "Stack")

[Stack](https://en.wikipedia.org/wiki/Stack_(abstract_data_type)) data structure implementation.

***

## What is stack?
Stack is a linear data structure, which has a particular order in which operations are performed.
The order is LIFO (Last In First Out), which means that last item which has been added to the stack, will be the first item that will be taken off the stack. Similarly first item which has been added to the stack, will be the last one to be removed.

There are four basic operations performed on stack:
- push: Adds new item to stack.
- pop: Removes item from stack.
- top: Returns last item added to stack.
- is_empty: Returns if stack is empty.

***

## Technical informations
**Build with**
- [CMake](https://cmake.org/)

**Written in**
- [C](https://en.cppreference.com/w/c/language)

**Tested with**
- [GTest](https://github.com/google/googletest)

***

## Basic usage
```C
#include <stdio.h>

#include "stack.h"

int main() {
    // Create stack
    StackT stack;

    // Initialize stack
    init(&stack, 10);

    // Add new item
    push(&stack, 1);

    // Get item which is on top
    ItemT* item = top(&stack);

    // Remove item
    pop(&stack);

    // Deinit stack
    deinit(&stack);
}
```

***

## How to use
Copy files from **/src** dir and include header file.

***

## Tests
Every function has been tested using [GTest](https://github.com/google/googletest). There are currently 32 tests in total.

***

## To do
- Create better tests or refactor existing.
- Change CMake script.
