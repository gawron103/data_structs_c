//==========================================================//
// FILE ENCODING: UTF-8                                     //
//==========================================================//

#ifndef STACK_H
#define STACK_H

//==========================================================//
// INCLUDES SECTION START                                   //
//==========================================================//
#include <stdbool.h>
#include <limits.h>
//==========================================================//
// INCLUDES SECTION END                                     //
//==========================================================//

//==========================================================//
// TYPES SECTION START                                      //
//==========================================================//
typedef bool CheckT;
typedef signed int PositionT;
typedef signed int CapacityT;
typedef signed int SizeT;
typedef int* ItemT;

typedef struct {
    ItemT data;
    PositionT top;
    SizeT size;
    CapacityT capacity;
} StackT;
//==========================================================//
// TYPES SECTION END                                        //
//==========================================================//

//==========================================================//
// CONSTANT SECTION START                                   //
//==========================================================//
#define E_NOT_OK             ((CheckT) 0u)
#define E_OK                 ((CheckT) 1u)

#define NOT_EMPTY            ((CheckT) 0u)
#define EMPTY                ((CheckT) 1u)

#define TOP_INIT_VAL         ((PositionT) -1)
#define SIZE_INIT_VAL        ((SizeT) 0)

#define TOP_INVALID_VAL      ((PositionT) INT_MIN)
#define SIZE_INVALID_VAL     ((SizeT) -1)
#define CAPACITY_INVALID_VAL ((CapacityT) -1)

#define NO_MEM_ALLOCATED     (NULL)

#define NO_ITEM              (NULL)
//==========================================================//
// CONSTANT SECTION END                                     //
//==========================================================//

//==========================================================//
// FUNCTION PROTOTYPES SECTION START                        //
//==========================================================//
CheckT init(StackT* const stack, const SizeT size);
CheckT deinit(StackT* const stack);
CheckT push(StackT* const stack, const int val);
CheckT pop(StackT* const stack);
CheckT is_empty(const StackT* const stack);
ItemT top(const StackT* const stack);
//==========================================================//
// FUNCTION PROTOTYPES SECTION END                          //
//==========================================================//

#endif // STACK_H

//==========================================================//
// EOF
//==========================================================//
