//==========================================================//
// FILE ENCODING: UTF-8                                     //
//==========================================================//

//==========================================================//
// INCLUDES SECTION START                                   //
//==========================================================//
#include <stdio.h>
#include <stdlib.h>

#include "stack.h"
//==========================================================//
// INCLUDES SECTION END                                     //
//==========================================================//

//==========================================================//
// FUNCTIONS IMPLEMENTATIONS SECTION START                  //
//==========================================================//

//==========================================================//
// DESCRIPTION: Function for aligning the desired memory for
//              stack and for setting it's attributes to 
//              default ones.
//
// PARAMETERS: stack: struct representing stack
//                    data structure.
//
// RETURN VALUE: E_OK: succesfull initialization.
//               E_NOT_OK: unsuccessful initialization.
//==========================================================//
CheckT init(StackT* const stack, const SizeT size) {
    CheckT status = E_NOT_OK;

    if(NO_MEM_ALLOCATED != stack) {
        stack->capacity = size;

        stack->data = (ItemT)malloc(sizeof(int) * stack->capacity);

        if(NO_MEM_ALLOCATED == stack->data) {
            fprintf(stderr, "Malloc failed in init\n");
        }

        stack->size = SIZE_INIT_VAL;
        stack->top = TOP_INIT_VAL;

        status = E_OK;
    }

    return status;
}

//==========================================================//
// DESCRIPTION: Function for dealocating space owned by
//              stack and setting it's attributes to invalid.
//
// PARAMETERS: stack: struct representing stack
//                    data structure.
//
// RETURN VALUE: E_OK: memory has been released.
//               E_NOT_OK: memory haven't been released.
//==========================================================//
CheckT deinit(StackT* const stack) {
    CheckT status = E_NOT_OK;

    if(NO_MEM_ALLOCATED != stack) {
        free(stack->data);

        // Set stack attributes to invalid
        stack->data = NO_MEM_ALLOCATED;
        stack->capacity = CAPACITY_INVALID_VAL;
        stack->size = SIZE_INVALID_VAL;
        stack->top = TOP_INVALID_VAL;

        status = E_OK;
    }

    return status;
}

//==========================================================//
// DESCRIPTION: Function for adding new items to stack.
//
// PARAMETERS: stack: struct representing stack
//                    data structure.
//
// RETURN VALUE: E_OK: the item was added to stack.
//               E_NOT_OK: the item wasn't added to
//                         the stack.
//==========================================================//
CheckT push(StackT* const stack, const int val) {
    CheckT status = E_NOT_OK;

    if(NO_MEM_ALLOCATED != stack) {
        if(stack->size != stack->capacity) {
            stack->top++;
            stack->data[stack->top] = val;
            stack->size++;

            status = E_OK;
        }
    }

    return status;
}

//==========================================================//
// DESCRIPTION: Function that removes the last added item
//              to the stack.
//
// PARAMETERS: stack: struct representing stack
//                    data structure.
//
// RETURN VALUE: E_OK: item has been removed
//                     from stack.
//               E_NOT_OK: item hasn't been removed
//                         from stack.
//==========================================================//
CheckT pop(StackT* const stack) {
    CheckT status = E_NOT_OK;

    if(NO_MEM_ALLOCATED != stack) {
        const CheckT IS_EMPTY = is_empty(stack);

        if(EMPTY != IS_EMPTY) {
            stack->top--;
            stack->size--;

            status = E_OK;
        }
    }

    return status;
}

//==========================================================//
// DESCRIPTION: Function that specifies if stack is empty.
//
// PARAMETERS: stack: struct representing stack
//                    data structure.
//
// RETURN VALUE: EMPTY: when stack has no items.
//               NOT_EMPTY: when stack has at least one item.
//==========================================================//
CheckT is_empty(const StackT* const stack) {
    CheckT status = EMPTY;

    if(NO_MEM_ALLOCATED != stack) {
        status = TOP_INIT_VAL < stack->top ? NOT_EMPTY : EMPTY;
    }

    return status;
}

//==========================================================//
// DESCRIPTION: Function that returns pointer to the top 
//              item in stack.
//
// PARAMETERS: stack: struct representing stack
//                    data structure.
//
// RETURN VALUE: ItemT: pointer to the last item added 
//                      to stack.
//==========================================================//
ItemT top(const StackT* const stack) {
    ItemT item = NO_ITEM;

    if(NO_MEM_ALLOCATED != stack) {
        const CheckT IS_EMPTY = is_empty(stack);

        if(EMPTY != IS_EMPTY) {
            item = &(stack->data[stack->top]);
        }
    }

    return item;
}

//==========================================================//
// FUNCTIONS IMPLEMENTATIONS SECTION END                    //
//==========================================================//

//==========================================================//
// EOF
//==========================================================//
