#include "gtest/gtest.h"

#ifdef __cplusplus
extern "C" {
#include "stack.h"
}
#else
#include "stack.h"
#endif

TEST(Stack_Destructor_Tests, deinitCheckVal) {
    const size_t WANTED_SIZE = 10u;
    CheckT deinit_retVal = E_NOT_OK;

    StackT stack;

    (void)init(&stack, WANTED_SIZE);
    deinit_retVal = deinit(&stack);

    EXPECT_EQ(deinit_retVal, E_OK);
}

TEST(Stack_Destructor_Tests, deinitCheckValWhenNullPassed) {
    const size_t WANTED_SIZE = 10u;
    CheckT deinit_retVal = E_NOT_OK;

    deinit_retVal = deinit(NULL);

    EXPECT_EQ(deinit_retVal, E_NOT_OK);
}

TEST(Stack_Destructor_Tests, structAfterDeInit) {
    const size_t WANTED_SIZE = 10u;

    StackT stack;

    (void)init(&stack, WANTED_SIZE);
    (void)deinit(&stack);    

    EXPECT_TRUE(stack.data == NO_MEM_ALLOCATED);
    EXPECT_EQ(stack.top, TOP_INVALID_VAL);
    EXPECT_EQ(stack.capacity, CAPACITY_INVALID_VAL);
    EXPECT_EQ(stack.size, SIZE_INVALID_VAL);
}
