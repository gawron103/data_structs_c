#include "gtest/gtest.h"

#ifdef __cplusplus
extern "C" {
#include "stack.h"
}
#else
#include "stack.h"
#endif

TEST(Stack_Push_Tests, pushCheckVal) {
    const int WANTED_SIZE = 3;
    CheckT pushRetVal = E_NOT_OK;
    const int TEST_VAL = 7;

    StackT stack;

    (void)init(&stack, WANTED_SIZE);
    pushRetVal = push(&stack, TEST_VAL);

    EXPECT_EQ(pushRetVal, E_OK);
}

TEST(Stack_Push_Tests, pushCheckValWhenNull) {
    CheckT pushRetVal = E_NOT_OK;
    const int TEST_VAL = 7;

    pushRetVal = push(NULL, TEST_VAL);

    EXPECT_EQ(pushRetVal, E_NOT_OK);
}

TEST(Stack_Push_Tests, pushMultipleVals) {
    const int WANTED_SIZE = 3;

    StackT stack;

    (void)init(&stack, WANTED_SIZE);

    for(int i = 0; i < WANTED_SIZE; i++) {
        (void)push(&stack, i);
    }

    EXPECT_EQ(stack.data[0], 0);
    EXPECT_EQ(stack.data[1], 1);
    EXPECT_EQ(stack.data[2], 2);
}

TEST(Stack_Push_Tests, pushTooMuchVals) {
    const int WANTED_SIZE = 3;
    CheckT pushRetVal = E_NOT_OK;

    StackT stack;

    (void)init(&stack, WANTED_SIZE);

    for(int i = 0; i < WANTED_SIZE + 1; i++) {
        pushRetVal = push(&stack, i);
    }

    EXPECT_EQ(stack.data[0], 0);
    EXPECT_EQ(stack.data[1], 1);
    EXPECT_EQ(stack.data[2], 2);
    EXPECT_EQ(pushRetVal, E_NOT_OK);
}

TEST(Stack_Push_Tests, checkTopPosition1) {
    const int WANTED_SIZE = 3;
    const int TEST_VAl = 7;

    StackT stack;

    (void)init(&stack, WANTED_SIZE);

    (void)push(&stack, TEST_VAl);

    EXPECT_EQ(stack.top, 0);
}

TEST(Stack_Push_Tests, checkTopPosition2) {
    const int WANTED_SIZE = 3;
    const int TEST_VAl = 7;

    StackT stack;

    (void)init(&stack, WANTED_SIZE);

    for(int i = 0; i < WANTED_SIZE; i++) {
        (void)push(&stack, TEST_VAl);
    }

    EXPECT_EQ(stack.top, 2);
}

TEST(Stack_Push_Tests, checkPushRetValWhenCapacityTZero) {
    const int WANTED_SIZE = 0;
    const int TEST_VAl = 7;
    CheckT pushRetVal = E_NOT_OK;

    StackT stack;

    (void)init(&stack, WANTED_SIZE);
    pushRetVal = push(&stack, TEST_VAl);

    EXPECT_EQ(pushRetVal, E_NOT_OK);
}

TEST(Stack_Push_Tests, checkStackAttribsAfterPushWhenCapacityTZero) {
    const int WANTED_SIZE = 0;
    const int TEST_VAl = 7;

    StackT stack;

    (void)init(&stack, WANTED_SIZE);
    (void)push(&stack, TEST_VAl);

    EXPECT_EQ(stack.capacity, WANTED_SIZE);
    EXPECT_EQ(stack.size, 0);
    EXPECT_EQ(stack.top, -1);
}
