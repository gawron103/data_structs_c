#include "gtest/gtest.h"

#ifdef __cplusplus
extern "C" {
#include "stack.h"
}
#else
#include "stack.h"
#endif

TEST(Stack_Top_Tests, topAfterInit) {
    const size_t WANTED_SIZE = 5u;
    ItemT ItemT = NULL;

    StackT stack;

    (void)init(&stack, WANTED_SIZE);

    ItemT = top(&stack);

    EXPECT_TRUE(ItemT == NULL);
}

TEST(Stack_Top_Tests, topAterDeinit) {
    const size_t WANTED_SIZE = 5u;
    ItemT ItemT = NULL;

    StackT stack;

    (void)init(&stack, WANTED_SIZE);
    (void)deinit(&stack);
    
    ItemT = top(&stack);

    EXPECT_TRUE(ItemT == NULL);
}

TEST(Stack_Top_Tests, topAfterPush) {
    const size_t WANTED_SIZE = 5u;
    ItemT ItemT = NULL;
    int TEST_PUSH_VAL = 6;

    StackT stack;

    (void)init(&stack, WANTED_SIZE);

    (void)push(&stack, TEST_PUSH_VAL);

    ItemT = top(&stack);

    EXPECT_TRUE(ItemT != NULL);
}

TEST(Stack_Top_Tests, topAfterPop) {
    const size_t WANTED_SIZE = 5u;
    ItemT ItemT = NULL;
    int TEST_PUSH_VAL = 6;

    StackT stack;

    (void)init(&stack, WANTED_SIZE);

    (void)push(&stack, TEST_PUSH_VAL);
    (void)pop(&stack);

    ItemT = top(&stack);

    EXPECT_TRUE(ItemT == NULL);
}

TEST(Stack_Top_Tests, valueChanged) {
    const size_t WANTED_SIZE = 5u;
    ItemT item = NULL;
    int TEST_PUSH_VAL = 3;

    StackT stack;

    (void)init(&stack, WANTED_SIZE);

    (void)push(&stack, TEST_PUSH_VAL);
    (void)push(&stack, (TEST_PUSH_VAL * TEST_PUSH_VAL));

    item = top(&stack);
    *item = -55;

    EXPECT_EQ(stack.data[stack.top], -55);
}
