#include "gtest/gtest.h"

#ifdef __cplusplus
extern "C" {
#include "stack.h"
}
#else
#include "stack.h"
#endif

TEST(Stack_Pop_Tests, popCheckVal) {
    const size_t WANTED_SIZE = 20u;
    const int TEST_VAL = 1;
    CheckT pop_retVal = E_NOT_OK;

    StackT stack;

    (void)init(&stack, WANTED_SIZE);
    (void)push(&stack, TEST_VAL);
    pop_retVal = pop(&stack);

    EXPECT_EQ(pop_retVal, E_OK);
}

TEST(Stack_Pop_Tests, popCheckValWhenPassNull) {
    CheckT pop_retVal = E_NOT_OK;

    pop_retVal = pop(NULL);

    EXPECT_EQ(pop_retVal, E_NOT_OK);
}

TEST(Stack_Pop_Tests, tooMuchPopCheckVal) {
    const size_t WANTED_SIZE = 20u;
    const int TEST_VAL = 1;
    CheckT pop_retVal = E_NOT_OK;

    StackT stack;

    (void)init(&stack, WANTED_SIZE);

    for(int i = 0; i < WANTED_SIZE; i++) {
        (void)push(&stack, TEST_VAL);
    }

    for(int i = 0; i < WANTED_SIZE + 1; i++) {
        pop_retVal = pop(&stack);
    }

    EXPECT_EQ(pop_retVal, E_NOT_OK);
}

TEST(Stack_Pop_Tests, checkStackAttribsAfterPop) {
    const size_t WANTED_SIZE = 20u;
    const int TEST_VAL = 1;

    StackT stack;

    (void)init(&stack, WANTED_SIZE);
    (void)push(&stack, TEST_VAL);

    EXPECT_EQ(stack.capacity, 20);
    EXPECT_EQ(stack.size, 1);
    EXPECT_EQ(stack.top, 0);

    (void)pop(&stack);

    EXPECT_EQ(stack.capacity, 20);
    EXPECT_EQ(stack.size, 0);
    EXPECT_EQ(stack.top, -1);
}

TEST(Stack_Pop_Tests, checkTopPositionT1) {
    const size_t WANTED_SIZE = 20u;
    const int TEST_VAL = 1;

    StackT stack;

    (void)init(&stack, WANTED_SIZE);
    for(int i = 0; i < WANTED_SIZE / 2; i++) {
        (void)push(&stack, TEST_VAL);
    }

    pop(&stack);
    pop(&stack);

    EXPECT_EQ(stack.top, 7);
}

TEST(Stack_Pop_Tests, checkTopPositionT2) {
    const size_t WANTED_SIZE = 20u;
    const int TEST_VAL = 1;

    StackT stack;

    (void)init(&stack, WANTED_SIZE);

    for(int i = 0; i < WANTED_SIZE; i++) {
        (void)push(&stack, TEST_VAL);
    }

    for(int i = 0; i < WANTED_SIZE; i++) {
        (void)pop(&stack);
    }

    EXPECT_EQ(stack.top, -1);
}

TEST(Stack_Pop_Tests, checkDataAfterPops) {
    const size_t WANTED_SIZE = 5u;

    StackT stack;

    (void)init(&stack, WANTED_SIZE);

    for(int i = 0; i < WANTED_SIZE; i++) {
        (void)push(&stack, i);
    }

    EXPECT_EQ(stack.data[0], 0);
    EXPECT_EQ(stack.data[1], 1);
    EXPECT_EQ(stack.data[2], 2);
    EXPECT_EQ(stack.data[3], 3);
    EXPECT_EQ(stack.data[4], 4);

    for(int i = 0; i < stack.size / 2; i++) {
        (void)pop(&stack);
    }

    for(int i = 0; i < stack.size; i++) {
        EXPECT_EQ(stack.data[i], i);
    }
}
