#include "gtest/gtest.h"

#ifdef __cplusplus
extern "C" {
#include "stack.h"
}
#else
#include "stack.h"
#endif

TEST(Stack_Empty_Tests, isEmptyAfterInit) {
    const size_t WANTED_SIZE = 10u;
    CheckT isEmpty = EMPTY;

    StackT stack;

    (void)init(&stack, WANTED_SIZE);

    isEmpty = is_empty(&stack);

    EXPECT_EQ(isEmpty, EMPTY);
}

TEST(Stack_Empty_Tests, isEmptyAfterDeinit) {
    const size_t WANTED_SIZE = 10u;
    CheckT isEmpty = EMPTY;

    StackT stack;

    (void)init(&stack, WANTED_SIZE);
    (void)deinit(&stack);

    isEmpty = is_empty(&stack);

    EXPECT_EQ(isEmpty, EMPTY);
}

TEST(Stack_Empty_Tests, isEmptyAfterPush) {
    const size_t WANTED_SIZE = 10u;
    const int TEST_VAL = 7;
    CheckT isEmpty = EMPTY;

    StackT stack;

    (void)init(&stack, WANTED_SIZE);
    (void)push(&stack, TEST_VAL);

    isEmpty = is_empty(&stack);

    EXPECT_EQ(isEmpty, NOT_EMPTY);
}

TEST(Stack_Empty_Tests, isEmptyAfterPop) {
    const size_t WANTED_SIZE = 10u;
    const int TEST_VAL = 7;
    CheckT isEmpty = EMPTY;

    StackT stack;

    (void)init(&stack, WANTED_SIZE);
    (void)push(&stack, TEST_VAL);
    (void)push(&stack, TEST_VAL);
    (void)pop(&stack);

    isEmpty = is_empty(&stack);

    EXPECT_EQ(isEmpty, NOT_EMPTY);
}

TEST(Stack_Empty_Tests, isEmptyAfterPopAll) {
    const size_t WANTED_SIZE = 10u;
    const int TEST_VAL = 7;
    CheckT isEmpty = EMPTY;

    StackT stack;

    (void)init(&stack, WANTED_SIZE);

    for(int i = 0; i < WANTED_SIZE; i++) {
        (void)push(&stack, TEST_VAL);
    }

    for(int i = 0; i < WANTED_SIZE; i++) {
        (void)pop(&stack);
    }

    isEmpty = is_empty(&stack);

    EXPECT_EQ(isEmpty, EMPTY);    
}
