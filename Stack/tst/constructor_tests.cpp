#include "gtest/gtest.h"

#ifdef __cplusplus
extern "C" {
#include "stack.h"
}
#else
#include "stack.h"
#endif

TEST(Stack_Constructor_Tests, initCheckVal) {
    const size_t WANTED_SIZE = 10u;
    CheckT init_retVal = E_NOT_OK;

    StackT stack;

    init_retVal = init(&stack, WANTED_SIZE);

    EXPECT_EQ(init_retVal, E_OK);
}

TEST(Stack_Constructor_Tests, initCheckValWhenNullPass) {
    const size_t WANTED_SIZE = 10u;
    CheckT init_retVal = E_NOT_OK;

    init_retVal = init(NULL, WANTED_SIZE);

    EXPECT_EQ(init_retVal, E_NOT_OK);
}

TEST(Stack_Constructor_Tests, structValuesAfterInit) {
    const size_t WANTED_SIZE = 10u;

    StackT stack;

    (void)init(&stack, WANTED_SIZE);

    EXPECT_TRUE(stack.data != NO_MEM_ALLOCATED);
    EXPECT_EQ(stack.top, TOP_INIT_VAL);
    EXPECT_EQ(stack.size, SIZE_INIT_VAL);
    EXPECT_EQ(stack.capacity, WANTED_SIZE);
}

TEST(Stack_Constructor_Tests, structInitWithAssignedVals) {
    const size_t WANTED_SIZE = 10u;

    StackT stack = {
        .data = NO_MEM_ALLOCATED,
        .top = 43,
        .size = 23,
        .capacity = 0
    };

    (void)init(&stack, WANTED_SIZE);

    EXPECT_TRUE(stack.data != NO_MEM_ALLOCATED);
    EXPECT_EQ(stack.top, TOP_INIT_VAL);
    EXPECT_EQ(stack.size, SIZE_INIT_VAL);
    EXPECT_EQ(stack.capacity, WANTED_SIZE);
}
